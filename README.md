# ASP .NETCORE MVC Application

## Introduction
The goal for this project is to initiate myself with **C# web programming** and to review **MVC architecture**.  
It works with **ASPNETCORE**, therefore it is compatible with linux. 

## First Steps
First step was to create my project using the **command line**  <br> 
>dotnet new mvc myProject   <br> 
## WalkThrough
Then I implemented a Model User, and started to implement registration procedure.
I could have implemented default Netcore Identity packages , but I preferred building something myself and keep it simple.
This app uses Cookies Authentifications.

## Main Components: 
User model, linked with a DBContext (UserContext)   <br> 
Register Page/ Viewmodel,strongly typed with validation. FrontEnd control both by ASP and jQuery   <br> 
Login Page with Claims gestion / ClaimsIdentity (roles implememented)

## TO INITIALISE THE DATABASE   <br> 
>dotnet ef migrations add DefaultContext  :Add migrations with instructions to build the tables   <br> 
>dotnet ef database update  Create/UPDATE the database
