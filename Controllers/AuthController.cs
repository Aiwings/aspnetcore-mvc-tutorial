
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

using Tutorial.ViewModels;
using Tutorial.Models;
using Tutorial.Data;




namespace Tutorial.Controllers
{
    [Route("auth")]
    public class AuthController : Controller
    {

        private readonly  UserContext _context;
        public AuthController(ILogger<AuthController> logger, UserContext context )
        {
            _context = context;
            this.logger = logger;
        }

        private ILogger logger ;
        
        
        [HttpGet]
        [Route("register")]
        public IActionResult Register()
        {

            return View();
        }

        [HttpPost]
        [Route("register")]
        [ValidateAntiForgeryToken]
        public ActionResult Register( RegisterViewModel user, [FromQuery] string test)
        {
            if(!ModelState.IsValid ) 
            {
                return View(user);
            } 

            var check = _context.Exists(user.Email);
            if(!check) 
            {
                User ident_user= new User() { Email = user.Email};
                ident_user.SetPassword(user.Password);
                _context.Users.Add(ident_user);
                if(_context.SaveChanges() != 0)
                {
                    return RedirectToAction("login");
                }
                else
                {

                   ModelState.AddModelError("db", "Erreur dans la base de données");
                    return View();
                }
            } 
            else
            {
                ModelState.AddModelError(user.Email, "Un utilisateur existe déjà avec cet email");
                return View();
            }
        }
        [HttpGet]
        [Route("login")]
        public IActionResult Login() {
            return View();
        }

        [HttpPost]
        [Route("login")]
        [ValidateAntiForgeryToken]
        public  async Task<IActionResult> Login(LoginVModel model,string ReturnUrl) {
            if(!ModelState.IsValid)
            {
                return View(model);
            }
            string hpw = Tutorial.Models.User.HashPasword(model.Password);
            using(_context)
            {
                var user = _context.GetUser(new User(){Email= model.Email, Password= hpw });
                if(user != null)
                {
                   return await Connect(user,ReturnUrl);
                } 
                else
                {
                    ModelState.AddModelError(user.Email, "Email / Mot de passe incorrect");
                    return View();
                }
            }
        }
        public async Task<IActionResult> Connect(User user, string ReturnUrl)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.Email),
                new Claim(ClaimTypes.Role, "Administrator"),
            };
            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperty = new AuthenticationProperties();

            await HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity)
            );
            return Redirect(ReturnUrl == null ? "/pannel" : ReturnUrl);

        }
    }
}