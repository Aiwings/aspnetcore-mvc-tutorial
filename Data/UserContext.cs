using Microsoft.EntityFrameworkCore;
using System.Linq;



using Tutorial.Models;

namespace Tutorial.Data
{
    public class UserContext :  DbContext
    {
        public UserContext(DbContextOptions<UserContext> options) :base(options)
        {

        }
        public DbSet<User> Users { get; set; } 

        public User GetUser(User user) {
            var obj = Users.Where(
                u => u.Email == user.Email && u.Password == user.Password
            ).FirstOrDefault();
            return obj;
        }

        public bool Exists(string email)
        {
            return (Users.FirstOrDefault(
                u => u.Email == email
            ) != null);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("User");
        }
        
    }

}