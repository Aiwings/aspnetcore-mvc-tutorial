jQuery(document).ready(function($) {
    //do jQuery stuff when DOM is ready
    var message;

    var validatePW = () => {
        isvalid = true;
        let pw = $("#Password").val();
        let pw_conf = $("#Pass_confirm").val();
        console.log(pw, pw_conf);
        if(!pw) {
            isvalid = false;
            message.push( "Veuillez saisir un mot de passe");
        } else if(!pw_conf) {
            isvalid = false;
            message.push("Confirmer votre mot de passe");
        } else if (pw_conf != pw) {
            isvalid = false;
            message.push("Les mots de passes ne correspondent pas");
        }
        return isvalid;     
    }
    var validateMail = () => {
        let isValid = true;
        let mail = $("#Email").val()
        if(!mail) {
            message.push("Veuillez saisir un email");
            isValid= false;
        } else {
        }
        return isValid;
    }
    
    $("#registerForm").submit( (event) => { 

        message = [];
        $("#err-show").hide(); 
        let validpw = validatePW();
        let validmail = validateMail();
        if(validpw && validmail) {
          return
        } else {
            console.log(message);
            let message_txt = message.join("<br/>");
            $("#err-show").html(message_txt);
            $("#err-show").show();
        }
        event.preventDefault();
    });
    var isMessageEmpty = () => {
        let message_html = $("#err-show").html().trim();
        if(message_html) {
            $("#err-show").show();  
        }
    }
    isMessageEmpty();

});