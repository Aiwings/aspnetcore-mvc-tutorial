using System.ComponentModel.DataAnnotations;

namespace Tutorial.ViewModels
{
    public class RegisterViewModel
    {
        [Required (AllowEmptyStrings = false, ErrorMessage = "L'email est requis")]
        [EmailAddress(ErrorMessage = "Adresse non valide")]
        [Display(Name = "E-mail")]
        public string Email {get;set;}
        
        [RegularExpression("(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9]).{7,}$",ErrorMessage = "Chiffre + Majuscules requis")]
        [StringLength(20, MinimumLength = 7,  ErrorMessage = "Minimum 7 caractères")]
        [DataType(DataType.Password)]
        [Required (AllowEmptyStrings = false, ErrorMessage = "Le mot de passe est requis")]
        [Display(Name = "mot de passe")]
        public string Password{get;set;}
        [DataType(DataType.Password)]
        [Required (AllowEmptyStrings = false, ErrorMessage = "Confirmez le mot de passe")]
        [Compare("Password", ErrorMessage = "Les mots de passe ne correspondent pas !")]
        [Display(Name = "Confirmez")]
        public string Pass_confirm{get;set;}
        
    }
}