using System.ComponentModel.DataAnnotations;
namespace Tutorial.ViewModels
{
    public class LoginVModel
    {   
        [Required (AllowEmptyStrings= false , ErrorMessage="L'email est requis")]
        [EmailAddress(ErrorMessage = "Adresse non valide")]
        [Display(Name="Adresse Email")]
        public string Email {get;set;}

        [DataType(DataType.Password)]
        [Required(ErrorMessage="Veuillez saisir un mot de passe")]
        [Display(Name="Mot de passe")]
        public string Password {get;set;}
        
    }
}