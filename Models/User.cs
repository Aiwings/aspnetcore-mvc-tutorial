using System;
using System.Text;
using System.ComponentModel.DataAnnotations;  
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography;


namespace Tutorial.Models
{
    public class User 
    {
        [Key, Column(Order = 1)]  
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)] 
        public int ID{get;set;}
        public string Firstrname{get;set;}

        public string Lastname {get;set;}
        [DataType(DataType.Date)]
        public DateTime? Date_birth{get;set;}

        [Required]
        public string Email {get;set;}
        [Required]
        public string Password {get;set;}

        public void SetPassword(string value)
        {
            Password = HashPasword(value);
        }
        public static string HashPasword(string value)
        {
            string res="";
            SHA256 shcrypt = SHA256.Create();
            byte[] hash = shcrypt.ComputeHash(Encoding.UTF8.GetBytes(value));
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                builder.Append(hash[i].ToString());
            }
            res = builder.ToString();
            return res;
        }
    }
}